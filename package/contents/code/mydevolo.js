function getUuid(json) {
    if (json.hasOwnProperty('uuid')) {
        return json.uuid;
    }
    else {
        return false;
    }
}

function getRequestUri(uri, type, username, password) {
    var url = plasmoid.configuration.url + "/v1/" + uri;
    return getRequest(url, type, username, password);
}

function getRequest(href, type, username, password) {
    var request = new XMLHttpRequest();
    
    console.log("Calling " + href);
    request.open('GET', href);
    
    if (username !== undefined && password !== undefined) {
        var auth = Qt.btoa(username + ":" + password);
        request.setRequestHeader("Authorization", "Basic " + auth);
    }
    
    return request;
}
 
function getJsonFromCloud(getHref, successCallback, failureCallback, isUri, username, password, model) {
    
    if (username === undefined) {
        username = plasmoid.configuration.devoloId;
    }
    if (password === undefined) {
        password = plasmoid.configuration.password;
    }
    
    if (isUri) {
        var request = getRequestUri(getHref, 'GET', username, password);
    }
    else {
        var request = getRequest(getHref, 'GET', username, password);
    }
    
    request.onreadystatechange = function () {
        if (request.readyState !== XMLHttpRequest.DONE) {
            return;
        }
        
        if (request.status !== 200) {
            console.log("Request to " + getHref + " failed");
            console.log("Response code: " + request.status);
            if (model === undefined) {
                failureCallback(request);
            }
            else {
                failureCallback(request, model);
            }
        }
        else {
            var json = JSON.parse(request.responseText);
            if (model === undefined) {
                successCallback(json);
            }
            else {
                successCallback(json, model);
            }
        }
    }
    request.send();
}

function callUrl(getHref, successCallback, failureCallback, name) {
    var request = getRequest(getHref, 'GET');
    
    request.onreadystatechange = function () {
        if (request.readyState !== XMLHttpRequest.DONE) {
            return;
        }
        
        if (request.status !== 200) {
            console.log("Request to " + getHref + " failed");
            console.log("Response code: " + request.status);
            failureCallback(name);
        }
        else {
            successCallback(name);
        }
    }
    request.send();
}
