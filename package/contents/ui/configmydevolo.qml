import QtQuick 2.0
import QtQuick.Controls 1.3 as QtControls
import QtQuick.Layouts 1.0
import org.kde.plasma.core 2.0 as PlasmaCore
import "../code/mydevolo.js" as MyDevolo

ColumnLayout {
    id: generalConfigPage
    
    property alias cfg_devoloId: devoloId.text
    property alias cfg_password: password.text
    property string cfg_gateway
    
    property int gatewayNo
    
    PlasmaCore.DataSource {
        id: notificationSource
        engine: "notifications"
        connectedSources: "org.freedesktop.Notifications"
    }
    
    function addGateways() {
        gatewayNo = 0;
        busyIndication.running = true;
        getUser();
    }
    
    function getUser() {
        MyDevolo.getJsonFromCloud("users/uuid", getGateways, loginFailed, true, devoloId.text, password.text);
    }
    
    function getGateways(userDetails) {
        var uuid = MyDevolo.getUuid(userDetails);
        var uri = "users/" + uuid + "/hc/gateways";
        MyDevolo.getJsonFromCloud(uri, getGatewayDetails, unexpectedError, true, devoloId.text, password.text);
    }
    
    function getGatewayDetails(gateways) {
        var items = gateways.items;
        gateway.remove(0);
        gatewayNo = items.length;
        for (var i=0; i<gatewayNo; i++) {
            MyDevolo.getJsonFromCloud(items[i].href, addGatewaysToView, unexpectedError, false, devoloId.text, password.text);
        }
    }
    
    function addGatewaysToView(gatewayDetails) {
        if (gatewayDetails.type==="M") {
            if (gatewayDetails.location === null) {
                gateway.append({"id": gatewayDetails.gatewayId, text: gatewayDetails.gatewayId});
                finalSteps();
            }
            else {
                MyDevolo.getJsonFromCloud(gatewayDetails.location, addGatewaysWithLocationToView, unexpectedError, false, devoloId.text, password.text, gatewayDetails);
            }
        }
        else {
            finalSteps();
        }
    }
    
    function addGatewaysWithLocationToView(location, gatewayDetails) {
        gateway.append({"id": gatewayDetails.gatewayId, text: location.name + " (" + gatewayDetails.gatewayId + ")"});
        finalSteps();
    }
    
    function finalSteps() {
        gatewayNo--;
        if (gatewayNo === 0) {
            dropdown.currentIndex = 0;
            for (var i=0; i<gateway.count; i++) {
                if (gateway.get(i).id === cfg_gateway) {
                    dropdown.currentIndex = i;
                    break;
                }
            }
            cfg_gateway = gateway.get(dropdown.currentIndex).id;
            busyIndication.running = false;
        }
    }
    
    function loginFailed() {
        var summary = i18n("Login Failed");
        var text = i18n("Incorrect user name and/or password.");
        createNotification(summary, text);
    }
    
    function unexpectedError() {
        var summary = i18n("Error");
        var text = i18n("An error has occurred.");
        createNotification(summary, text);
    }
    
    function createNotification(summary, text) {
        var service = notificationSource.serviceForSource("notification");
        var operation = service.operationDescription("createNotification");

        operation.appName = "devolo Home Control";
        operation["appIcon"] = "dialog-error";
        operation.summary = summary;
        operation["body"] = text
        operation["timeout"] = 5000;

        service.startOperationCall(operation);
    }
        
    GridLayout {
        columns: 3
        
        QtControls.Button {
            id: loginButton
            Layout.row: 0
            Layout.column: 3
            Layout.alignment: Qt.AlignRight
            text: i18n("Sign in")
            onClicked: addGateways()
        }
        
        QtControls.BusyIndicator {
            id: busyIndication
            Layout.row: 1
            Layout.column: 3
            Layout.rowSpan: 2
            Layout.alignment: Qt.AlignCenter
            running: false;
        }

        QtControls.Label {
            Layout.row: 0
            Layout.column: 0
            Layout.alignment: Qt.AlignRight
            text: i18n("devolo ID")
        }
        
        QtControls.TextField {
            id: devoloId
            Layout.row: 0
            Layout.column: 1
            Layout.minimumWidth: units.gridUnit * 16
            echoMode: TextInput.Normal
        }
        
        QtControls.Label {
            Layout.row: 1
            Layout.column: 0
            Layout.alignment: Qt.AlignRight
            text: i18n("Password")
        }
        
        QtControls.TextField {
            id: password
            Layout.row: 1
            Layout.column: 1
            Layout.minimumWidth: units.gridUnit * 16
            echoMode: TextInput.Password
        }
        
        QtControls.Label {
            Layout.row: 2
            Layout.column: 0
            Layout.alignment: Qt.AlignRight
            text: i18n("Central Unit")
        }
        
        QtControls.ComboBox {
            id: dropdown
            Layout.row: 2
            Layout.column: 1
            Layout.minimumWidth: units.gridUnit * 16
            textRole: "text"
            model: ListModel {
                id: gateway
            }
            onCurrentIndexChanged: {
                cfg_gateway = gateway.get(currentIndex).id
            }
        }
    }
    
    Item {
        Layout.fillHeight: true
    }
    
    Component.onCompleted: {
	Qt.application.name = "devolohcscenes"
        if (plasmoid.configuration.devoloId.length!=0 && plasmoid.configuration.password.length!=0) {
            devoloId.text = plasmoid.configuration.devoloId
            password.text = plasmoid.configuration.password
            addGateways()
        }
        else {
            gateway.append({text: i18n("Please sign in first.")})
        }
    }
}
