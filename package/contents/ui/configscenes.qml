import QtQuick 2.0
import QtQuick.Controls 1.3 as QtControls
import QtQuick.Layouts 1.0
import org.kde.plasma.core 2.0 as PlasmaCore
import "../code/mydevolo.js" as MyDevolo

ColumnLayout {
    id: generalConfigPage
    
    property string cfg_scenes
    property var scenes
    
    PlasmaCore.DataSource {
        id: notificationSource
        engine: "notifications"
        connectedSources: "org.freedesktop.Notifications"
    }
    
    function addScenes() {        
        scenes = [];
        getUser();
    }
    
    function getUser() {
        MyDevolo.getJsonFromCloud("users/uuid", getScenes, loginFailed, true);
    }
    
    function getScenes(userDetails) {
        var uuid = MyDevolo.getUuid(userDetails);
        var uri = "users/" + uuid + "/hc/gateways/" + plasmoid.configuration.gateway + "/scenes/actions";
        MyDevolo.getJsonFromCloud(uri, getActions, unexpectedError, true);
    }
    
    function getActions(scenes) {
        var items = scenes.items;
        for (var i=0; i<items.length; i++) {
            MyDevolo.getJsonFromCloud(items[i].href, getActionDetails, unexpectedError, false);
        }
    }
    
    function getActionDetails(details) {
        var detailsString = JSON.stringify(details);
        Qt.createQmlObject('
            import QtQuick 2.0; 
            import QtQuick.Controls 1.0 as QtControls; 
            QtControls.CheckBox { 
                signal send(bool state, string details); 
                text: "' + details.sceneName + '";
                checked: inConfig("' + details.sceneName + '");
                onClicked: { send(checked, "' + encodeURI(detailsString) + '") }
                Component.onCompleted: { send.connect(addScene) }
            }', 
            sceneSelection);
    }
    
    function inConfig(name) {
        if (cfg_scenes !== "") {
            var config = JSON.parse(cfg_scenes);
            for (var i=0; i<config.length; i++) {
                if (config[i].sceneName === name) {
                    return true;
                }
            }
        }
        return false;
    }
    
    function addScene(checked, details) {
        details = decodeURI(details);
        var detailsObject = JSON.parse(details);
        if (checked === true) {
            scenes.push(detailsObject);
        }
        else {
            scenes = scenes.filter(function(scene) {
                return scene.sceneName != detailsObject.sceneName;
            });
        }
        cfg_scenes = JSON.stringify(scenes);      
    }
    
    function loginFailed() {
        var summary = i18n("Login Failed");
        var text = i18n("Incorrect user name and/or password.");
        createNotification(summary, text);
    }
    
    function unexpectedError() {
        var summary = i18n("Error");
        var text = i18n("An error has occurred.");
        createNotification(summary, text);
    }
    
    function createNotification(summary, text) {
        var service = notificationSource.serviceForSource("notification");
        var operation = service.operationDescription("createNotification");

        operation.appName = "devolo Home Control";
        operation["appIcon"] = "dialog-error";
        operation.summary = summary;
        operation["body"] = text
        operation["timeout"] = 5000;

        service.startOperationCall(operation);
    }
    
    QtControls.GroupBox {
        flat: true       
        Column {
            id: sceneSelection
        }
    }
    
    Item {
        Layout.fillHeight: true
    }
    
    Component.onCompleted: {
	Qt.application.name = "devolohcscenes"
        if (plasmoid.configuration.devoloId.length!=0 && plasmoid.configuration.password.length!=0) {
            addScenes();
        }
        else {
            Qt.createQmlObject('
            import QtQuick 2.0; 
            Text { 
                text: i18n("Please sign in first.");
            }', 
            sceneSelection);
        }    
    }
}
