import QtQuick 2.2
import QtQuick.Controls 1.3 as QtControls
import QtQuick.Layouts 1.0
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras

Item { 
    width: units.gridUnit * 10
    height: units.gridUnit * 10
    Plasmoid.preferredRepresentation: plasmoid.fullRepresentation
    Layout.minimumWidth: units.gridUnit * 12 * (plasmoid.formFactor == PlasmaCore.Types.Horizontal ? sourcesModel.count : 1)
    Layout.minimumHeight: units.gridUnit * 4 * (plasmoid.formFactor == PlasmaCore.Types.Vertical ? sourcesModel.count : 1)
    Layout.preferredHeight: Layout.minimumHeight

    Component.onCompleted: {
        Qt.application.name = "devolohcscenes"
        if (plasmoid.configuration.scenes.length!=0) {
            addScenes()
        }
    }    
    Plasmoid.onUserConfiguringChanged: {
        if (plasmoid.configuration.scenes.length!=0) {
            sceneList.children = ''
            addScenes()
        }
    }
    
    PlasmaCore.DataSource {
        id: notificationSource
        engine: "notifications"
        connectedSources: "org.freedesktop.Notifications"
    }
     
    function addScenes() {
        var scenes = JSON.parse(plasmoid.configuration.scenes);
        for (var i=0; i<scenes.length; i++) {
            Qt.createQmlObject('
                import QtQuick 2.0; 
                import "../code/mydevolo.js" as MyDevolo; 
                Row {
                    spacing: 5;
                    Image {
                        source: (areaimage.containsMouse || areatext.containsMouse) ? "../images/playhover.png" : "../images/play.png";
                        MouseArea {
                            id: areaimage; 
                            anchors.fill: parent; 
                            hoverEnabled: true; 
                            cursorShape: Qt.PointingHandCursor; 
                            onClicked: MyDevolo.callUrl("' + scenes[i].link + '", successfulCall, failedCall, "' + scenes[i].sceneName + '");
                        }
                    } 
                    Text {
                        text: "' + scenes[i].sceneName + '";
                        color: theme.textColor;
                        anchors.verticalCenter: parent.verticalCenter;
                        MouseArea {
                            id: areatext; 
                            anchors.fill: parent; 
                            hoverEnabled: true; 
                            cursorShape: Qt.PointingHandCursor; 
                            onClicked: MyDevolo.callUrl("' + scenes[i].link + '", successfulCall, failedCall, "' + scenes[i].sceneName + '");
                        }
                    }                 
                }',
                sceneList
            );
        }
    }
    
    function successfulCall(name) {
        var summary = i18n("Playback the scene");
        var text = i18n("The scene \"%1\" was successfully launched and will soon be executed.", name);
        var icon = Qt.resolvedUrl('../images/mydevolo.png').replace('file://', '');
        createNotification(summary, text, icon);
    }
    
    function failedCall(name) {
        var summary = i18n("Playback the scene");
        var text = i18n("The scene \"%1\" was not successfully launched!", name);
        var icon = "dialog-error";
        createNotification(summary, text, icon);
    }
    
    function createNotification(summary, text, icon) {
        var service = notificationSource.serviceForSource("notification");
        var operation = service.operationDescription("createNotification");

        operation.appName = "devolo Home Control";
        operation["appIcon"] = icon;
        operation.summary = summary;
        operation["body"] = text
        operation["timeout"] = 5000;

        service.startOperationCall(operation);
    }
    
    PlasmaExtras.Heading {
        id: heading
        width: parent.width
        level: 2
        text: "devolo Home Control"
    }
    
    GridLayout {
        id: sceneList
        columns: 1
        rows: 1

        anchors {
            top: heading.bottom
            topMargin: 10
        }
    }
}
