import QtQuick 2.1
import org.kde.plasma.configuration 2.0

ConfigModel {
    ConfigCategory {
        name: "my devolo"
        icon: Qt.resolvedUrl('../images/cloud.png').replace('file://', '')
        source: "configmydevolo.qml"
    }

    ConfigCategory {
        name: i18n("Scenes")
        icon: Qt.resolvedUrl('../images/scenes.png').replace('file://', '')
        source: "configscenes.qml"
    }
}
