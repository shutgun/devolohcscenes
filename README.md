# devolo Home Control scenes plasmoid for KDE

This plasmoid enables you to playback scenes shared with devolo Home Control by a click on your desktop.

## License

The plasmoid is distributed under GPL version 3. See LICENSE for details. The used images are copyrighted by devolo AG and licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. See LICENSE.images for details.

## Runtime Requirements
* KDE Framework >=5.18.0
* QT >=5.5

## Build Requirements
* cmake >=3.5.1
* extra-cmake-modules >=5.18
* gcc >=5.3.1
* gettext >=0.19.7

Depending on your distribution you might need additional packages. For e.g. kubuntu:

```
apt install cmake extra-cmake-modules g++ plasma-framework-dev qtbase5-dev qtdeclarative5-dev
```

## Installation

### Get source
```
git clone git@gitlab.com:shutgun/devolohcscenes.git
cd devolohcscenes
```

### Local installation
```
plasmapkg2 -i package
```

to install the plasmoid. Translations will be ignored in this case.

### Global installation
```
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr -Wno-dev
make
sudo make install
```

Available translations will be used in this case.
